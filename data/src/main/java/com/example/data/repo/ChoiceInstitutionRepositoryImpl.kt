package com.example.data.repo

import com.example.domain.data.InstitutionItemType
import com.example.domain.repository.ChoiceInstitutionRepository

class ChoiceInstitutionRepositoryImpl : ChoiceInstitutionRepository {
    override fun getInstitution(): List<InstitutionItemType> = listOf(
        InstitutionItemType("РГПУ им. Герцена") {},
        InstitutionItemType("Московский технический университет связи и информатики") {},
        InstitutionItemType("Московский финансово-промышленный университет «Синергия»") {},
        InstitutionItemType("Национальный исследовательский ядерный университет «МИФИ»") {},
        InstitutionItemType("Государственный академический университет гуманитарных наук") {},
        InstitutionItemType("Российский университет дружбы народов") {},
        InstitutionItemType("Институт современного искусства") {},
        InstitutionItemType("Московская высшая школа социальных и экономических наук") {},
        InstitutionItemType("Московский политехнический университет") {},
        InstitutionItemType("Институт международных экономических связей") {},
        InstitutionItemType("Московская академия предпринимательства") {},
        InstitutionItemType("Международный юридический институт") {}
    )
}
