package com.example.data

const val BASE_URL = "https://fakerapi.it/api/v1/"

const val USERS = "users"

const val QUANTITY = "_quantity"
const val GENDER = "_gender"