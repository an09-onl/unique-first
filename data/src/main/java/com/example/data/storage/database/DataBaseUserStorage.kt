package com.example.data.storage.database

interface DataBaseUserStorage {

    suspend fun getUsers():List<UserEntity>

    suspend fun insertUsers(userEntity: UserEntity)
}