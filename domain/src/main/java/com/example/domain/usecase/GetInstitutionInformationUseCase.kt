package com.example.domain.usecase

import com.example.domain.base.UseCase
import com.example.domain.data.InstitutionItemType
import com.example.domain.repository.ChoiceInstitutionRepository

class GetInstitutionInformationUseCase(private val choiceInstitutionRepository: ChoiceInstitutionRepository) :
    UseCase<Unit, List<InstitutionItemType>> {

    override fun execute(param: Unit?): List<InstitutionItemType> {
        return choiceInstitutionRepository.getInstitution()
    }
}