package com.example.domain.data

data class InstitutionItemType(
    val name: String,
    val action: () -> Unit
)