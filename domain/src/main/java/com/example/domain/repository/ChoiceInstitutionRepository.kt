package com.example.domain.repository

import com.example.domain.data.InstitutionItemType

interface ChoiceInstitutionRepository {

    fun getInstitution(): List<InstitutionItemType>
}
