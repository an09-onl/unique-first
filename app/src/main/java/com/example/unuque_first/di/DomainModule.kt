package com.example.unuque_first.di

import com.example.domain.usecase.GetInstitutionInformationUseCase
import com.example.domain.usecase.GetUsersUseCase
import org.koin.dsl.module

val domainModule = module {

    factory<GetInstitutionInformationUseCase> { GetInstitutionInformationUseCase(get()) }

    factory<GetUsersUseCase> { GetUsersUseCase(get())}

}
