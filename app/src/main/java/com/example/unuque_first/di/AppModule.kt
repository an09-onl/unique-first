package com.example.unuque_first.di

import com.example.unuque_first.ui.fragments.choiceInstitution.ChoiceInstitutionViewModel
import com.example.unuque_first.ui.fragments.user.UserViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    viewModel<ChoiceInstitutionViewModel> {
        ChoiceInstitutionViewModel(get())
    }
    viewModel<UserViewModel>{
        UserViewModel(get())
    }
}

