package com.example.unuque_first.list

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.viewbinding.ViewBinding
import com.example.unuque_first.base.BaseViewHolder
import com.example.domain.data.InstitutionItemType
import com.example.unuque_first.databinding.FragmentInstitutionBinding

class Adapter : ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(
    object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is InstitutionItemType && newItem is InstitutionItemType -> oldItem == newItem
            else -> false
        }

        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is InstitutionItemType && newItem is InstitutionItemType -> oldItem.name == newItem.name
            else -> false
        }
    }
) {
    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is InstitutionItemType -> FIRST_TYPE
        else -> throw IllegalArgumentException(
            "Adapter can`t handle the item ${
                getItem(
                    position
                )
            }"
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        FIRST_TYPE -> InstitutionViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw IllegalArgumentException("Adapter can`t handle the item with this type $viewType")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val FIRST_TYPE = 999
    }
}

private class InstitutionViewHolder(parent: ViewGroup) :
    BaseViewHolder<FragmentInstitutionBinding, InstitutionItemType>(
        FragmentInstitutionBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    override fun FragmentInstitutionBinding.bind(value: InstitutionItemType) {
        title.text = value.name
        itemView.setOnClickListener {
            value.action()
        }
    }
}
