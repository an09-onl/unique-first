package com.example.unuque_first.ui.fragments.timetable

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentTimetableBinding
import com.example.unuque_first.ui.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class TimetableFragment : BaseFragment<FragmentTimetableBinding>() {

    //private val viewModel by viewModel<TimetableViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentTimetableBinding = FragmentTimetableBinding.inflate(inflater, container, false)


    override fun FragmentTimetableBinding.onBindView(saveInstanceState: Bundle?) {
       // viewModel.timetableliveData.observe(viewLifecycleOwner) {

            addTimetable.setOnClickListener {
                navController.navigate(TimetableFragmentDirections.navigateToGroupInstitutionFragment())
            }
        }
    }
//}