package com.example.unuque_first.ui.fragments.choiceInstitution

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentChoiceInstitutionBinding
import com.example.unuque_first.list.Adapter
import com.example.unuque_first.ui.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ChoiceInstitutionFragment : BaseFragment<FragmentChoiceInstitutionBinding>() {

    private val viewModel by viewModel<ChoiceInstitutionViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentChoiceInstitutionBinding =
        FragmentChoiceInstitutionBinding.inflate(inflater, container, false)

    override fun FragmentChoiceInstitutionBinding.onBindView(saveInstanceState: Bundle?) {
        back.setOnClickListener {
            navController.popBackStack()
        }

        val newAdapter = Adapter()
        newAdapter.submitList(viewModel.getInstitution())

        viewModel.choiceInstitutionLiveData.observe(viewLifecycleOwner) {
            list.adapter = newAdapter
        }
    }
}


