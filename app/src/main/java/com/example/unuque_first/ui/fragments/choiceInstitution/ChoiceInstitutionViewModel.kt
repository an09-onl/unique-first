package com.example.unuque_first.ui.fragments.choiceInstitution

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.domain.data.InstitutionItemType
import com.example.domain.usecase.GetInstitutionInformationUseCase

class ChoiceInstitutionViewModel (private val getInstitutionInformationUseCase: GetInstitutionInformationUseCase) :
    ViewModel() {

    private val _choiceInstitutionLiveData = MutableLiveData<List<InstitutionItemType>>()
    val choiceInstitutionLiveData: LiveData<List<InstitutionItemType>> = _choiceInstitutionLiveData

    fun getInstitution(): List<InstitutionItemType> {
        val result = getInstitutionInformationUseCase.execute()
        _choiceInstitutionLiveData.value = listOf()
        return result
    }
}