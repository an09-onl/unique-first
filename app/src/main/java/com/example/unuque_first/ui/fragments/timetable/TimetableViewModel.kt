package com.example.unuque_first.ui.fragments.timetable

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TimetableViewModel : ViewModel() {

    private val _timetableliveData = MutableLiveData<String>()
    val timetableliveData: LiveData<String> = _timetableliveData
}