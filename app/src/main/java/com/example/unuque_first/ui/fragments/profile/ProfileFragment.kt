package com.example.unuque_first.ui.fragments.profile

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentProfileBinding
import com.example.unuque_first.ui.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class ProfileFragment : BaseFragment<FragmentProfileBinding>(){

     //private val viewModel by viewModel<ProfileViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentProfileBinding = FragmentProfileBinding.inflate(inflater,container,false)

    override fun FragmentProfileBinding.onBindView(saveInstanceState: Bundle?) {
        //viewModel.profileLiveData.observe(viewLifecycleOwner){
        profileSettings.setOnClickListener {
            navController.navigate( ProfileFragmentDirections.navigateToUserFragment())
        }
    }
}