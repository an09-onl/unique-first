package com.example.unuque_first.ui.fragments.today

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentTodayBinding
import com.example.unuque_first.ui.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class TodayFragment : BaseFragment<FragmentTodayBinding>() {

    //private val viewModel by viewModel<TodayViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentTodayBinding = FragmentTodayBinding.inflate(inflater, container, false)


    override fun FragmentTodayBinding.onBindView(saveInstanceState: Bundle?) {

        //viewModel.todayLiveData.observe(viewLifecycleOwner) {
            addTimetable.setOnClickListener {
                navController.navigate(TodayFragmentDirections.navigateToNavigationTimetable())
            }

            loginOrRegistration.setOnClickListener {
                navController.navigate(TodayFragmentDirections.navigateToNavigationProfile())
            }
        }
    }
//}