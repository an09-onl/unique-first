package com.example.unuque_first.ui.fragments.user

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.viewbinding.ViewBinding
import com.example.unuque_first.base.BaseViewHolder
import androidx.recyclerview.widget.ListAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.domain.models.UserDto
import com.example.unuque_first.databinding.UserItemBinding

class UserAdapter :
    ListAdapter<Any, BaseViewHolder<ViewBinding, Any>>(object : DiffUtil.ItemCallback<Any>() {
        override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is UserDto && newItem is UserDto -> oldItem.id == newItem.id
            else -> false
        }

        @SuppressLint("DiffUtilEquals")
        override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean = when {
            oldItem is UserDto && newItem is UserDto -> oldItem == newItem
            else -> false
        }
    }) {

    override fun getItemViewType(position: Int): Int = when (getItem(position)) {
        is UserDto -> USER_ITEM_TYPE
        else -> throw java.lang.IllegalArgumentException(
            "UserAdapter can't handle item" + getItem(
                position
            )
        )
    }

    override fun onCreateViewHolder(
        parent: ViewGroup, viewType: Int
    ): BaseViewHolder<ViewBinding, Any> = when (viewType) {
        USER_ITEM_TYPE -> UserViewHolder(parent) as BaseViewHolder<ViewBinding, Any>
        else -> throw java.lang.IllegalArgumentException("UserAdapter can't handle the $viewType type")
    }

    override fun onBindViewHolder(holder: BaseViewHolder<ViewBinding, Any>, position: Int) =
        holder.handleItem(getItem(position))

    companion object {
        private const val USER_ITEM_TYPE = 997
    }
}

private class UserViewHolder(private val parent: ViewGroup) :
    BaseViewHolder<UserItemBinding, UserDto>(
        UserItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
    ) {
    @SuppressLint("SetTextI18n")
    override fun UserItemBinding.bind(value: UserDto) {
        firstname.text = "Имя: " + value.firstname
        lastname.text = "Фамилия: " + value.lastname
        email.text = "E-mail: " + value.email

        Glide.with(parent.context)
            .load(value.image)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .centerCrop().into(imageView)
    }
}