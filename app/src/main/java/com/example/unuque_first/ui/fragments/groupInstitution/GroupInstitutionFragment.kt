package com.example.unuque_first.ui.fragments.groupInstitution

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentGroupInstitutionBinding
import com.example.unuque_first.ui.fragments.base.BaseFragment
import com.example.unuque_first.ui.fragments.choiceInstitution.ChoiceInstitutionViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class GroupInstitutionFragment : BaseFragment<FragmentGroupInstitutionBinding>() {

    //private val viewModel by viewModel<GroupInstitutionViewModel>()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentGroupInstitutionBinding = FragmentGroupInstitutionBinding.inflate(inflater, container, false)

    override fun FragmentGroupInstitutionBinding.onBindView(saveInstanceState: Bundle?) {

        // viewModel.groupInstitutionLiveData.observe(viewLifecycleOwner) {

        institutionChoice.setOnClickListener {
            navController.navigate (GroupInstitutionFragmentDirections.navigateToChoiceInstitutionFragment())
        }
        back.setOnClickListener {
            navController.popBackStack()
        }
    }
}
//}
