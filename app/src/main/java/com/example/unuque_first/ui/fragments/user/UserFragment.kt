package com.example.unuque_first.ui.fragments.user

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.unuque_first.databinding.FragmentUserBinding
import com.example.unuque_first.ui.fragments.base.BaseFragment
import org.koin.androidx.viewmodel.ext.android.viewModel

class UserFragment : BaseFragment<FragmentUserBinding>() {

    private val viewModel by viewModel<UserViewModel>()

    private val userAdapter = UserAdapter()

    override fun createViewBinding(
        inflater: LayoutInflater,
        container: ViewGroup?
    ): FragmentUserBinding = FragmentUserBinding.inflate(inflater, container, false)

    override fun FragmentUserBinding.onBindView(saveInstanceState: Bundle?) {
        viewModel.getUsers(4)
        userList.adapter = userAdapter

        viewModel.userLiveData.observe(viewLifecycleOwner) { user ->
            userAdapter.submitList(user)
        }
    }
}
